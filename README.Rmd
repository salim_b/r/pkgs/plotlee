---
output: pal::gitlab_document
---

```{r}
#| label: init
#| include: false

knitr::opts_knit$set(root.dir = getwd())

library(rlang,
        include.only = "%|%")
library(magrittr,
        include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))
```

# `r pal::desc_value("Package")`

```{r}
#| label: pkg-desc
#| results: asis
#| echo: false

pal::cat_lines(paste0("[![CRAN Status](https://r-pkg.org/badges/version/", pal::desc_value(key = "Package"), ")](https://cran.r-project.org/package=",
                      pal::desc_value(key = "Package"), "){.pkgdown-release}"),
               "",
               stringr::str_replace_all(string = pal::desc_value("Description"),
                                        pattern = c("plotly package" = "[plotly](https://plotly-r.com/) package",
                                                    "plotly.js" = "[plotly.js](https://plotly.com/javascript/)")))
```

```{r}
#| label: pkg-doc
#| eval: !expr '!isTRUE(getOption("pal.build_readme.is_pkgdown"))'
#| results: asis
#| echo: false
pkgsnip::md_snip(id = "pkgdown_site") %>%
  paste0("## Documentation\n\n",
         "[![Netlify Status](https://api.netlify.com/api/v1/badges/f1183707-47e4-4767-ac4d-d52ec0200a38/deploy-status)]",
         "(https://app.netlify.com/sites/plotlee-rpkg-dev/deploys)\n\n",
         .) %>%
  pal::cat_lines()
```

## Installation

```{r}
#| label: pkg-instl-dev
#| child: !expr pkgsnip::snip_path("pkg-instl-dev-gitlab.Rmd")
```

```{r}
#| label: pkg-usage
#| eval: !expr isTRUE(getOption("pal.build_readme.is_pkgdown"))
#| results: asis
#| echo: false
pkgsnip::md_snip(id = "pkg_usage") %>%
  paste0("## Usage\n\n", .) %>%
  pal::cat_lines()
```

## Development

### R Markdown format

```{r}
#| label: pkgpurl
#| child: !expr pkgsnip::snip_path("pkgpurl.Rmd")
```

### Coding style

```{r}
#| label: pkg-code-style
#| child: !expr pkgsnip::snip_path("pkg-code-style.Rmd")
```
